// Single Line Comments (Ctrl + /)

/* 
	Multi line comments 
	-covers multiple line of comments
	(Crtl + Shift + /)
*/ 
let productName = 'desktop computer';

console.log(productName);

let productPrice = 18999;

console.log(productPrice);


productName = 'Laptop';

console.log(productName);

const interest = 3.539;

console.log(interest);

let supplier;

supplier = "John Smith Tradings"

console.log(supplier);

let productCode = "DC017";
const productBrand = "Dell"

console.log(productCode, productBrand);

let country = 'Philippines';
let province = "Metro Manila";

let fullAddress = province + ", " + country;

console.log(fullAddress);

console.log("Welcome to "+fullAddress+"!");

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";

console.log(message);
message = 'John\'s employess went home early';
console.log(message);

let headcount =26;
console.log(headcount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;

console.log(planetDistance);

console.log("John's grade last quarter is " + grade);


let isMarried = true;
let isGoodConduct = false;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grade);

let person = {
	Name: "Jeff",
	Age: "24",
	isMarried: false,
	contact: ["09195678910", "09992828822"],
	address: {
		houseNumber: 458,
		city: "Marikina"
	}
}

console.log(person);

let spouse = null;

let fullName = undefined;

// function printName(){
// 	console.log("My name is John");
// }

// printName();

function myAnimal(){
	console.log("My favorite animal is a dog");
}

myAnimal();


//(name) - parameter
//parameter - it acts as a named variable/container that exists only inside of a function

function printName(name){
	console.log("My name is " + name);
}

printName("John");

function argumentFunction(){
	console.log("This function");
}

function invokeFunction(argumentFunction){
	argumentFunction();
	console.log(argumentFunction);
}

invokeFunction(argumentFunction);

function createFullName(firstName, middleName, lastName){
	console.log("Hello " + firstName + " " + middleName + " " + lastName);
}

createFullName("Jeff Michael","Austria","Gomez");
createFullName("Juan","Gomez");
createFullName("Jeff Michael","Austria","Gomez", "Junior");


function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	console.log("A simple message");
}
